<?php
declare(strict_types=1);

namespace BusinessDecision\Component\Transformer\Context;


class Context implements ContextInterface
{
    /**
     * @var array|null
     */
    private $payload;

    /**
     * @param mixed|null $payload
     */
    public function __construct($payload = null)
    {
        $this->payload = $payload;
    }

    /**
     * @return array|null
     */
    public function getPayload(): ?array
    {
        return $this->payload;
    }
}
