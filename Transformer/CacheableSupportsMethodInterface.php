<?php


namespace BusinessDecision\Component\Transformer\Transformer;


interface CacheableSupportsMethodInterface
{
    public function hasCacheableSupportsMethod(): bool;
}
